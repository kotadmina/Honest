
//
//  CHAPI.swift
//  Choice
//
//  Created by ADMIN on 20.07.16.
//  Copyright © 2016 STANISLAV SVOROVSKY. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

let version = "v1_0_1"


enum CHMethod: String {
    // Users
    case Users					= "/choice-users"
    case LoginMethod			= "/choice-users/login"
    case SocialLoginMethod		= "/choice-users/social-login"
    case FacebookLoginMethod	= "/choice-users/fb-login"
    case VKLoginMethod			= "/choice-users/vk-login"
    
    case AddUserMethod			= "/choice-users/add"
    case UserUpdate				= "/choice-users/update"
    case UserSearch				= "/choice-users/search"
    case FollowersForDirect		= "/choice-users/followers-for-direct"
    case UserDataAvailable		= "/choice-users/is-available"
    case IsRegisteredSocial		= "/choice-users/is-registered-social"
    case IsBoundSocial          = "/choice-users/is-bound"
    
    case UpdatePassword			= "/choice-users/reset-password"
    case ValidatePassword		= "/choice-users/validate-password"
    case ResetPassword			= "/choice-users/forgot-password"
    
    case UserSubscribe			= "/choice-users/subscribe"
    case UserUnsubscribe		= "/choice-users/unsubscribe"
    
    case UserSubscribers		= "/choice-users/followers"
    case UserSubscriptions		= "/choice-users/subscriptions"
    
    case DirectInbox			= "/choice-users/inbox"
    case DirectOutbox			= "/choice-users/outbox"
    
    case GetFriendsFromSocial   = "/choice-users/social-friends"
    
    case RegistrationInfo		= "/choice-users/registration-info"
    
    case BindSocial             = "/choice-users/bind-social"
    case UnbindSocial           = "/choice-users/unbind-social"
    case IsSocial               = "/choice-users/is-social"
    case SocialBindingsNumber   = "/choice-users/social-bindings-num" 
    
    // ID
    case UserInfo				= "/choice-users/%d"
    case UserInfoLogin			= "/choice-users/view/%@"
    case UserLogout				= "/choice-users/%d/logout"
    
    // Choices
    
    case Feed					= "/polls/feed"
    case ChoiceSearch			= "/polls/search"
    case VotedChoices			= "/polls/voted"
    case InterestingChoices		= "/polls/ranked"
    case UserChoices			= "/choice-users/%d/polls"
    
    
    // Choice
    case CreateChoice			= "/polls/add"
    case ChoiceSend				= "/polls/send-poll"
    
    case ChoiceInfo				= "/polls/%d"
    case ChoiceOptions			= "/polls/%d/options"
    case ChoiceStatistics		= "/polls/%d/statistics"
    case ChoiceComments			= "/polls/%d/comments"
    
    case ChoiceVoteCities       = "/polls/%d/vote-cities"
    
    case ChoiceVote				= "/polls/%d/vote"
    case ChoiceDelete			= "/polls/%d/soft-delete"
    case ChoiceClose			= "/polls/%d/close-poll"
    case ChoiceReport			= "/polls/%d/report"
    
    case ChoiceCloseStatistics	= "/polls/%d/close-statistic"
    case ChoiceOpenStatistics	= "/polls/%d/open-statistic"
    case HideChoice             = "/polls/%d/delete-from-interesting"

    // Options

    case OptionInfo				= "/options/%d"
    case OriginalImage			= "/options/%d/original-image"

    // Themes
    case Themes					= "/categories"
    case ThemesSubscribe		= "/choice-users/category-subscribe"
    case ThemesUnsubscribe		= "/choice-users/category-unsubscribe"
    
    // Comment
    
    case CommentAdd				= "/comments/add"
    case CommentGet				= "/comments/%d"
    case CommentDelete			= "/comments/%d/delete"
    case CommentReport			= "/comments/%d/report"
    
    
    // Settings
    case Policy                 = "/privacies/policy"
    case Agreement              = "/privacies/agreement"
    
    func by(id: Int) -> String {
        if !self.rawValue.contains("%d") {
            return self.rawValue
        } else {
            return String(format: self.rawValue, id)
        }
    }
    
    func by(str: String) -> String {
        if !self.rawValue.contains("%@") {
            return self.rawValue
        } else {
            return String(format: self.rawValue, str)
        }
    }
 
    var needsID: Bool {
        return self.rawValue.contains("%d")
    }
    
    func HTTPMethod () -> Alamofire.HTTPMethod {
        switch self {

            // login
        case .UserDataAvailable,
             .IsRegisteredSocial,
             .IsSocial,
             .IsBoundSocial:
            return .get
        case .UpdatePassword,
             .ValidatePassword,
             .ResetPassword:
            return .post
        case .LoginMethod,
             .UserLogout,
             .FacebookLoginMethod,
             .VKLoginMethod,
             .SocialLoginMethod:
            return .put
            
            //choices
        case .DirectInbox,
             .VotedChoices,
             .DirectOutbox,
             .InterestingChoices,
             .Feed,
             .UserChoices,
             .ChoiceSearch,
             .ChoiceVoteCities,
             .ChoiceInfo,
             .ChoiceOptions,
             .ChoiceStatistics,
             .OptionInfo,
             .ChoiceComments,
             .OriginalImage:
            return .get
            
            // users
    	case .Users,
             .UserSubscribers,
             .UserSearch,
             .UserSubscriptions,
             .FollowersForDirect,
             .GetFriendsFromSocial,
             .UserInfo,
             .UserInfoLogin,
             .RegistrationInfo,
             .SocialBindingsNumber:
            return .get
            
        case .BindSocial,
             .UnbindSocial:
            return .post
            
            // themes
        case .Themes:
            return .get
            
        case .UserSubscribe,
             .ThemesSubscribe,
             .UserUnsubscribe,
             .ThemesUnsubscribe:
            return .put
            
            // creation
        case .CreateChoice,
             .AddUserMethod,
             .UserUpdate:
            return .post
            
			// puts
        case .ChoiceVote,
             .ChoiceCloseStatistics,
             .ChoiceSend,
             .ChoiceOpenStatistics,
             .ChoiceDelete,
             .ChoiceReport, // POST
             .ChoiceClose,
             .HideChoice:
            return .put
            
            // comments
        case .CommentGet:
            return .get
        case .CommentAdd:
            return .post
        case .CommentReport,
             .CommentDelete:
            return .put
            
            
        case .Agreement,
             .Policy:
            return .get
        }
    }
    
//    func DataType () -> JSONCollectionSerializable.Type? {
//        switch self {
//        case .VotedChoices,
//             .DirectInbox,
//             .DirectOutbox,
//             .Feed,
//             .ChoiceSearch,
//             .InterestingChoices,
//             .UserChoices:
//            return Choice.self
//        case .Users,
//             .UserSearch,
//             .FollowersForDirect,
//             .UserSubscribers,
//             .UserSubscriptions:
//            return CHOtherUser.self
//        case .ChoiceVoteCities:
//            return CHCity.self
//        default:
//            return nil
//        }
//    }
    
    
}

extension CHMethod: URLConvertible {
    func asURL() throws -> URL {
        guard let url = URL(string: rawValue) else {
            throw AFError.invalidURL(url: self)
        }
        return url
    }
}

extension CHMethod {
    func request( handle: ErrorHandleMethod = .alert, params: [String: AnyObject], completion: @escaping (JSON) -> Void = {_ in} ) {
        let _ = CHAPI.request(self.HTTPMethod(), self.rawValue, params: params, errorHandle: handle, success: completion)
    }
    func get( _ id: Int, handle: ErrorHandleMethod = .alert, params: [String: AnyObject], completion: @escaping (JSON) -> Void = {_ in} ) {
        let _ = CHAPI.request(self.HTTPMethod(), self.by(id: id), params: params, errorHandle: handle, success: completion)
    }
    func get( _ str: String, handle: ErrorHandleMethod = .alert, params: [String: AnyObject], completion: @escaping (JSON) -> Void = {_ in} ) {
        let _ = CHAPI.request(self.HTTPMethod(), self.by(str: str), params: params, errorHandle: handle, success: completion)
    }
}

protocol IDable {
    var id: Int { get set }
}

class CHAPI {
    static var manager: Alamofire.SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        return Alamofire.SessionManager(configuration: configuration)
    }()

    static let networkReachability: NetworkReachabilityManager? = {
        let manager = NetworkReachabilityManager(host: "api.choiceapp.ru")
        return manager
    }()
    
    static var _isReachable: Bool? = nil
    static var isReachable: Bool? {
        if let isReachable = _isReachable {
            return isReachable
        } else if let network = networkReachability {
            return network.isReachable
        } else {
            return nil
        }
    }
    
    static var hash: String? {
        return UserDefaultsManager().getHash()
    }
    
    static var base: String {
        return serverName + version
    }
    
    static var baseURL: URL? {
        return URL(string: base)
    }
    
    static func url(_ path: String) -> URL? {
        return URL(string: base + path)
    }
    
    static var serverName: String {
        return UserDefaultsManager().getServerName() ?? ""
    }
    
    static func hashed(parameters params: [String: AnyObject]? = nil) -> [String: AnyObject]? {
        var hashed = params ?? [:]
        
        if let hash = hash {
            hashed["hash"] = hash as AnyObject
        }
        return hashed

    }

    class func checkConnection(_ handler: @escaping (_ exists: Bool) -> Void) {
        if let network = networkReachability, !network.isReachable {
            handler(false)
            _isReachable = false
            return
        }
        
        _isReachable = nil
        guard let url = URL(string: base + "/tests/ping") else {
            handler(false)
            return
        }
        manager.request(url, method: .get).responseSwiftyJSON{
            response in
            let connectionExists: Bool = (response.result.error == nil)
                && (response.result.value?["response"].string == "Success!")
            // retry ?
            _isReachable = connectionExists
            handler(connectionExists)
        }
    }
    
    class func request(	_ method: Alamofire.HTTPMethod = .get,
                       	_ path: URLConvertible,
                    	params: [String: AnyObject]? = nil,
                    	hashed isHashed: Bool = true)
        -> DataRequest {
            
            let urlString = try! base + path.asURL().absoluteString
            let hashedParameters = isHashed ? hashed(parameters: params) : params
            let url = URL(string: urlString)!
            
            return manager.request(url, method: method, parameters: hashedParameters)
                .validate(contentType: ["application/json"])
    }

    class func request(	_ method: Alamofire.HTTPMethod = .get,
                       	_ path: URLConvertible,
                       	  params: [String: AnyObject]? = nil,
                       	  hashed isHashed: Bool = true,
                             error errorHandler: @escaping (CHError) -> Void = {_ in},
                             then finishHandler: (() -> Void)? = nil,
                             success completionHandler: @escaping (_ json: JSON) -> Void )
        -> DataRequest {
            
            let request = self.request(method, path, params: params, hashed: isHashed)
            print(request)
            return request.responseSwiftyJSON {
                response in
                
                let onError = { (error: ServerError) in
                    let error = CHError(error, status: response.response?.statusCode, data: response.result.value)
                    print("ERROR", error)
                    errorHandler( error )
                    finishHandler?()
                    
                    return
                }
                
                let json = response.result.value
                
                print("REQUEST", response.request)
                print("RESULT", response.result.value)
                print("PARAMS", params)
                
                switch response.result {
                case .success:
                    if let json = json, json["response"].exists() {
                        completionHandler(json["response"])
                    } else if let code = json?["code"].int, code != 0 {
                        onError(ServerError(rawValue: code) ?? .serverError)
                    } else if let code = json?["code"].int, code == 0 {
                        onError(.serverError)
                    } else {
                        onError(.badResponse)
                    }

                    finishHandler?()
                    
                case .failure(let error):
                    print("ERROR", error);
                    print("RESPONSE.RESULT.VALUE", response.result.value)

                    if error._code == NSURLErrorTimedOut || error._code == NSURLErrorNotConnectedToInternet {
                        print("Timeout! Check if connection exists")
                        checkConnection{
                            exists in
                            if exists {
                                onError(.timeout)
                            } else {
                                onError(.badConnection)
                            }
                        }
                    } else if let code = response.result.value?["code"].int, code != 0 {
                        onError(ServerError(rawValue: code) ?? .serverError)
                    } else if let code = response.result.value?["code"].int, code == 0 {
                        onError(.serverError)
                    } else {
                        onError(.badResponse)
                    }

                }
            }
    }
    
    
    class func request(	_ method: Alamofire.HTTPMethod = .get,
                       	_ path: URLConvertible,
                        params: [String: AnyObject]? = nil,
                        hashed isHashed: Bool = true,
                        errorHandle errorHandler: ErrorHandleMethod = .alert,
                        success completionHandler: @escaping (_ json: JSON) -> Void )
        -> DataRequest {
            return request(
                method, path, params: params, hashed: isHashed,
                error: { error in
                    errorHandler.exec(error)
                },
                success: completionHandler)
    }
    
    class func request(	_ method: Alamofire.HTTPMethod = .get,
                       	_ path: URLConvertible,
                       	  params: [String: AnyObject]? = nil,
                       	  hashed isHashed: Bool = true,
                             completion completionHandler: @escaping (_ json: JSON, _ error: CHError?) -> Void )
        -> DataRequest {
            
            return request(
                method, path, params: params, hashed: isHashed,
                error: {
                    error in
                    completionHandler([:], error)
                },
                then: nil,
                success: {
                    json in
                    completionHandler(json, nil)
                }
            )
            
    }
    
    class func request(	_ method: Alamofire.HTTPMethod = .get,
                       	_ path: URLConvertible,
                        params: [String: AnyObject]? = nil,
                        data: [String : Data]?,
                        hashed isHashed: Bool = true,
                        completion completionHandler: @escaping (_ json: JSON, _ error: CHError?) -> Void,
                        progress: ((_ progress: Double) -> Void)? = nil) {
        
        let urlString: String = try! base + path.asURL().absoluteString
        let hashedParameters = isHashed ? hashed(parameters: params) : params
        
        let fillFormData = { (formData: MultipartFormData) in
            let resources = data ?? [:]
            let parameters = hashedParameters ?? [:]
            
            var index = 0
            for (key, value) in resources {
                formData.append(value, withName: key, fileName: "file"+String(index)+".jpg", mimeType: "image/jpeg")
                index += 1
            }
            
            for (key, value) in parameters {
                let str = value as? String ?? String(describing: value)
                
                if let data = str.data(using: String.Encoding.utf8) {
                    formData.append(data, withName: key)
                }
            }
		}
        let url = URL(string: urlString)!
        Alamofire.upload(multipartFormData: fillFormData, to: url) { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(queue: DispatchQueue.global(qos: .utility), closure: { pr in
                        progress?(pr.fractionCompleted)
                    })
                    .responseSwiftyJSON { response in
                        debugPrint(response)
                        
                        // dealWith(response:  func would be usable here, as the next code appears a few times around here
                        
                        let onError = { (error: ServerError) in
                            let error = CHError(error, status: response.response?.statusCode, data: response.result.value)
                            print("ERROR", error)
                            completionHandler([:], error)
                            
                            return
                        }
                        
                        let json = response.result.value
                        
                        print("REQUEST", response.request ?? "")
                        print("RESULT", response.result.value ?? "")
                        print("PARAMS", hashedParameters ?? "")
                        
                        switch response.result {
                        case .success:
                            if let json = json, json["response"].exists() {
                                completionHandler(json["response"], nil)
                            } else if let code = json?["code"].int, code != 0 {
                                onError( ServerError(rawValue: code) ?? .serverError )
                            } else if let code = json?["code"].int, code == 0 {
                                onError(.serverError)
                            } else {
                                onError(.badResponse)
                            }
                            
                        case .failure(let error):
                            print("ERROR", error);
                            print("RESPONSE.RESULT.VALUE", response.result.value ?? "")
                            
                            if error._code == NSURLErrorTimedOut || error._code == NSURLErrorNotConnectedToInternet {
                                print("Timeout! Check if connection exists")
                                checkConnection{
                                    exists in
                                    if exists {
                                        onError(.timeout)
                                    } else {
                                        onError(.badConnection)
                                    }
                                }
                            } else if let code = response.result.value?["code"].int, code != 0 {
                                onError( ServerError(rawValue: code) ?? .serverError )
                            } else if let code = response.result.value?["code"].int, code == 0 {
                                onError(.serverError)
                            } else {
                                onError(.badResponse)
                            }
                            
                        }
                        
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    completionHandler([:], CHError(.badResponse))
                }

        }

    }
    
    class func list<T:JSONCollectionSerializable>(
        _ path: CHMethod, id: Int = 0, params: [String:AnyObject]? = nil,
        error errorHandler: @escaping (CHError) -> Void = {_ in},
    	then finishHandler: (() -> Void)? = nil,
        completion completionHandler: @escaping ([T]) -> Void ) {
        
        let method = path.HTTPMethod()
        let urlString = ( path.needsID ) ? path.by(id: id) : path.rawValue
        guard let url = URL(string: urlString) else {
            errorHandler(CHError(rawCode: ServerError.unknownError.rawValue))
            finishHandler?()
            return
        }
        let _ = request(method, url, params: params).responseCollection { (response: DataResponse<[T]>) in
            
            switch response.result {
            case .success(let val):
                completionHandler(val)
            case .failure(let error):
                errorHandler(CHError(error))
            }
            
            finishHandler?()
        }
    }
    
    class func list<T:JSONCollectionSerializable>(
        _ path: CHMethod, id: Int = 0, params: [String:AnyObject]? = nil,
        errorHandle errorHandler: ErrorHandleMethod = .alert,
        completionHandler: @escaping ([T]) -> Void ) {
        
        list(
            path, id: id, params: params,
            error: { (error) in
            	errorHandler.exec(error)
            },
            completion: completionHandler)
    }
    
    class func get<T:JSONSerializable>(
        _ path: CHMethod, id: Int = 0, params: [String:AnyObject],
        error errorHandler: @escaping (CHError) -> Void = {_ in},
        then finishHandler: (() -> Void)? = nil,
        completion completionHandler: @escaping (T) -> Void ) {
        
        let method = path.HTTPMethod()
        let urlString = ( path.needsID ) ? path.by(id: id) : path.rawValue
        guard let url = URL(string: urlString) else {
            errorHandler(CHError(rawCode: ServerError.unknownError.rawValue))
            finishHandler?()
            return
        }
        
        let _ = request( method, url, params: params).responseParse { (response: DataResponse<T>) in
            
            switch response.result {
            case .success(let val):
                completionHandler(val)
            case .failure(let error):
                errorHandler(CHError(error))
            }
            
            finishHandler?()
        }

    }
    
    class func get<T:JSONSerializable>(
        _ path: CHMethod, id: Int = 0, params: [String:AnyObject],
        errorHandle errorHandler: ErrorHandleMethod = .alert,
        completionHandler: @escaping (T) -> Void ) {
        
        get(
            path, id: id, params: params,
            error: { (error) in
                errorHandler.exec(error)
            },
            completion: completionHandler)
    }
}


public struct CHError: Error {
    var code: ServerError
    var status: StatusCode?
    var data: JSON?
    
    var value: Int {
        return code.rawValue
    }
    
    init(_ err: ServerError = .noError, status statusCode: Int? = nil, data: JSON? = nil) {
        self.code = err
        if let scode = statusCode {
            self.status = StatusCode(rawValue: scode)
        }
        self.data = data
    }
    
    init(rawCode: Int, status statusCode: Int? = nil, data: JSON? = nil) {
        self.init(ServerError(rawValue: rawCode) ?? .unknownError, status: statusCode, data: data)
    }
    
    init(_ error: NSError, status statusCode: Int? = nil, data: JSON? = nil) {
        self.init(rawCode: error.code, status: statusCode, data: data)
    }
    
    init(_ error: Error, status statusCode: Int? = nil, data: JSON? = nil) {
        self.init(rawCode: (error as NSError).code, status: statusCode, data: data)
    }
}

extension CHError: CustomStringConvertible {
    public var description: String {
        var arr: [String] = ["code: ", String(code.value), "\n"]
        if let statusValue = status?.rawValue {
            arr.append(contentsOf: ["server code: ", String(statusValue), "\n"])
        }
        if let dataDescription = data?.description {
            arr.append(contentsOf: ["data: ", dataDescription, "\n"])
        }
        
        return arr.reduce("", { $0 + $1 } )
    }
}


public protocol JSONSerializable {
    init(json: JSON)
}
public protocol JSONCollectionSerializable {
    static func collection(_ json: [JSON]) -> [Self]
}
extension JSONCollectionSerializable where Self: JSONSerializable {
    public static func collection(_ json: [JSON]) -> [Self] {
        var collection = [Self]()
        
        for item in json {
            collection.append( Self(json: item) )
        }
        
        return collection
    }
}

//extension Choice: JSONSerializable, JSONCollectionSerializable {
//    public convenience init(json: JSON) {
//        print(json)
//
//        self.init(dict: json.dictionaryObject as [String : AnyObject]? ?? [:])
//    }
//}

//extension CHOtherUser: JSONSerializable, JSONCollectionSerializable {
//    internal convenience init(json: JSON) {
//        self.init()
//        print(json)
//        userID        = json["id"].intValue
//        name        = json["first_name"].stringValue
//        surname        = json["last_name"].stringValue
//        login        = json["login"].stringValue
//
//        let urlString = json["avatar_url"].stringValue
//        if !urlString.isEmpty {
//            photoURL = URL(string: NSDictionary.getImageStoreURL() + urlString)!
//            SDWebImageManager.shared().downloadImage(with: photoURL, options: .retryFailed, progress: { _, _ in
//            }, completed: { image, _, _, _, _ in
//                    self.photo = image
//            })
//        }
//
//        isFollowedByUser = json["followed_by_viewer"].boolValue
//    }
//}

extension DataRequest {
    
    public func responseParse<T: JSONSerializable>(_ completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        let responseSerializer = DataResponseSerializer<T> { request, response, data, error in
            print("REQUEST DUMP")
            print("\trequest", request)
            print("\tresponse", response)
            print("\tdata", data)
            print("\terror", error)

            let json = try! JSON(data: data ?? Data())
            
            let onError = { (error: ServerError) -> Result<T> in
                let error = CHError(error, status: response?.statusCode, data: json)
                //completionHandler( json: [:], error: error )
                
                return .failure(error)
            }
            
            if let json = json["response"].arrayValue.first {
                let responseObject = T(json: json)
                return .success(responseObject)
                
                //completionHandler(json: json["response"], error: nil)
            } else if let error = error {
                if error._code == NSURLErrorTimedOut {
                    return onError(.timeout)
                } else if error._code == NSURLErrorNotConnectedToInternet {
                    return onError(.badConnection)
                } else {
                    return onError(.badResponse)
                }
            } else if let code = json["code"].int, code != 0 {
                return onError(ServerError(rawValue: code) ?? .serverError)
            } else if let code = json["code"].int, code == 0 {
                return onError(.serverError)
            } else {
                return onError(.badResponse)
            }
        }
        return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
    
    public func responseCollection<T: JSONCollectionSerializable>(_ completionHandler: @escaping (DataResponse<[T]>) -> Void) -> Self {
        let responseSerializer = DataResponseSerializer<[T]> { request, response, data, error in
            print("REQUEST DUMP")
            print("\trequest", request)
            print("\tresponse", response)
            print("\tdata", data)
            print("\terror", error)
            
            guard error == nil else {
                return .failure(CHError(.serverError))
            }
            
            guard let data = data else {
                return .failure(CHError(.badResponse))
            }
            
            if let responseData = try! JSON(data: data)["response"].array {
                let responseObject = T.collection(responseData)
                return .success(responseObject)
            } else {
                return .failure(CHError(.badResponse))
            }
        }
        
        return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
    
    
}
extension DataRequest {
    
    
    /**
     Wait for the request to finish then return the response value.
     
     - returns: The response.
     */
    public func response() -> (request: URLRequest?, response: HTTPURLResponse?, data: Data?, error: Error?) {
        
        let semaphore = DispatchSemaphore(value: 0)
        var result: (request: URLRequest?, response: HTTPURLResponse?, data: Data?, error: Error?)!
        
        self.response(queue: DispatchQueue.global(qos: .default)) { defaultDataResponse in
            result = (
                request: defaultDataResponse.request,
                response: defaultDataResponse.response,
                data: defaultDataResponse.data,
                error: defaultDataResponse.error
            )
            semaphore.signal()
        }
        
        let _ = semaphore.wait(timeout: DispatchTime.distantFuture)
        
        return result
    }
    
    
    /**
     Wait for the request to finish then return the response value.
     
     - returns: The response.
     */
    public func responseData() -> DataResponse<Data> {
        
        let semaphore = DispatchSemaphore(value: 0)
        
        var result: DataResponse<Data>!
        
        self.responseData(queue: DispatchQueue.global(qos: .default), completionHandler: { response in
            result = response
            semaphore.signal()
        })
        
        let _ = semaphore.wait(timeout: DispatchTime.distantFuture)
        
        return result
    }
    
    
    /**
     Wait for the request to finish then return the response value.
     
     - parameter options: The JSON serialization reading options. `.AllowFragments` by default.
     
     - returns: The response.
     */
    public func responseJSON(options: JSONSerialization.ReadingOptions = .allowFragments) -> DataResponse<Any> {
        
        let semaphore = DispatchSemaphore(value: 0)
        
        var result: DataResponse<Any>!
        
        self.responseJSON(queue: DispatchQueue.global(qos: .default), options: options, completionHandler: {response in
            result = response
            semaphore.signal()
        })
        
        let _ = semaphore.wait(timeout: DispatchTime.distantFuture)
        
        return result
    }
    
    
    /**
     Wait for the request to finish then return the response value.
     
     - parameter encoding: The string encoding. If `nil`, the string encoding will be determined from the
     server response, falling back to the default HTTP default character set,
     ISO-8859-1.
     
     - returns: The response.
     */
    public func responseString(encoding: String.Encoding? = nil) -> DataResponse<String> {
        
        let semaphore = DispatchSemaphore(value: 0)
        
        var result: DataResponse<String>!
        
        self.responseString(queue: DispatchQueue.global(qos: .default), encoding: encoding, completionHandler: { response in
            result = response
            semaphore.signal()
        })
        
        let _ = semaphore.wait(timeout: DispatchTime.distantFuture)
        
        return result
    }
    
    
    /**
     Wait for the request to finish then return the response value.
     
     - parameter options: The property list reading options. `0` by default.
     
     - returns: The response.
     */
    public func responsePropertyList(options: PropertyListSerialization.ReadOptions = PropertyListSerialization.ReadOptions()) -> DataResponse<Any> {
        
        let semaphore = DispatchSemaphore(value: 0)
        
        var result: DataResponse<Any>!
        
        self.responsePropertyList(queue: DispatchQueue.global(qos: .default), options: options, completionHandler: { response in
            result = response
            semaphore.signal()
        })
        
        let _ = semaphore.wait(timeout: DispatchTime.distantFuture)
        
        return result
    }
}


