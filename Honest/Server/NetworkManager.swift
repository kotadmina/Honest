//
//  NetworkManager.swift
//  Honest
//
//  Created by Sher Locked on 26.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager {
    
    static func getAPIUrl() -> String? {
        guard let url = URL(string: Constants.apiUrl) else {
            return nil
        }
        let response = Alamofire.request(url, method: .get).responseString()
        
        guard let value = response.result.value, let baseUrl = URL(string: value) else {
            return nil
        }
        return baseUrl.absoluteString
    }
    
}
