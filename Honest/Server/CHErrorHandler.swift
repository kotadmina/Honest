//
//  CHErrorHandler.swift
//  Choice
//
//  Created by SMIT on 12.07.16.
//  Copyright © 2016 STANISLAV SVOROVSKY. All rights reserved.
//

import Foundation

enum ErrorHandleMethod {
    case ignore
	case alert
    case popup
    case handler( (CHError) -> Void )
    case retry( Int )
    
    func check( _ error: CHError? ) -> Bool {
        if let error = error {
            self.exec(error)
            return false
        } else {
            // ignore
            return true
        }
    }
    
    func exec( _ error: CHError ) {
//        switch self {
//        case .ignore:
//            break
//        case .alert:
//            CHPopupView.showPopupWithErrorMessageFromKey("error code: \(error.value)")
//        case .popup:
//            CHPopupView.showAlert(withKey: "error code: \(error.value)")
//        case .retry( _):
//            fatalError("ErrorHandleMethod.Retry not implemented yet")
//        case .handler(let block):
//            block( error )
//        }
    }
}

class CHErrorHandler {
    
    class func handle(error: CHError, method: ErrorHandleMethod = .alert) {
        method.exec(error)
    }

    class func handle(error: CHError?) {
        print( "error", error)
        handleError(withMessage: error?.data?["message"].stringValue, errorCode: error?.value ?? 0 )
    }
    
    class func handleError(withMessage message: String?, errorCode error: Int?) {
        
        if message == nil && error == nil { return }
        
//        if message == nil {
//            CHPopupView.showPopupWithErrorMessage(NSLocalizedString("Error code: ", comment: "Alert") + String(error!), title: NSLocalizedString("Unknown error.", comment: "Alert"))
//            return
//        }
//
//        let message = message!
//
//
//        if message.contains("email value is being set to") {
//            CHPopupView.showPopupWithErrorTitleAndMessageFromKey("User with such email exists.")
//            return
//        }
//
//        if message.contains("login value is being set to") {
//            CHPopupView.showPopupWithErrorTitleAndMessageFromKey("User with such username exists.")
//            return
//        }
//
//        if message.contains("phone value is being set to") {
//            CHPopupView.showPopupWithErrorTitleAndMessageFromKey("User with such phone exists.")
//            return
//        }
//
//        if message == "{\"response\":{\"deleted\":[],\"active\":[\"email\"]}}" {
//            CHPopupView.showPopupWithErrorTitleAndMessageFromKey("User with such email exists.")
//            return
//        }
//        if message == "{\"response\":{\"deleted\":[],\"active\":[\"login\"]}}" {
//            CHPopupView.showPopupWithErrorTitleAndMessageFromKey("User with such username exists.")
//            return
//        }
//
//
//        CHPopupView.showPopupWithErrorMessage(message + ".\n" + NSLocalizedString("Error code: ", comment: "Alert") + String(error!), title: NSLocalizedString("Unknown error.", comment: "Alert"))
//
    }
}
