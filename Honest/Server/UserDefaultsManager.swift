//
//  UserDefaultsManager.swift
//  Honest
//
//  Created by Sher Locked on 28.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import Foundation

class UserDefaultsManager {
    
    private let hashKey = "hash"
    private let userIDKey = "user_id"
    private let serverNameKey = "serverName"
    
    func setHash(hash: String) {
        UserDefaults.standard.set(hash, forKey: hashKey)
    }
    
    func getHash() -> String? {
        return UserDefaults.standard.string(forKey: hashKey)
    }
    
    func setUserID(userID: Int) {
        UserDefaults.standard.set(userID, forKey: userIDKey)
    }
    
    func getUserID() -> Int? {
        return UserDefaults.standard.object(forKey: userIDKey) as? Int
    }
    
    func setServerName(serverName: String) {
        UserDefaults.standard.set(serverName, forKey: serverNameKey)
    }
    
    func getServerName() -> String? {
        return UserDefaults.standard.string(forKey: serverNameKey)
    }
    
}
