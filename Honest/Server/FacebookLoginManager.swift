//
//  FacebookLoginManager.swift
//  Honest
//
//  Created by Sher Locked on 26.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import SwiftyJSON
import Alamofire
import GooglePlaces

class FacebookLoginManager: NSObject {
    
    weak var delegate: FacebookLoginManagerDelegate?
    
    func performLogin() {
        let fields = ["id","gender","first_name","last_name","email","birthday","hometown","location"]
        let path = "me"
        let params = ["fields": "[\"" + fields.joined(separator: "\",\"") + "\"]"]
        let token = FBSDKAccessToken.current()
        
        FBSDKGraphRequest(graphPath: path, parameters: params).start { (_, response, error) in
            guard let response = response, error == nil else {
                // TODO: Handle error
                return
            }
            
            let userJSON = JSON(response)
            let id = userJSON["id"].stringValue
            self.checkIfRegistered(id: id, token: token?.tokenString ?? "", completion: { registered in
                guard let registered = registered else {
                    //TODO: Handler error
                    return
                }
                if registered {
                    self.performLoginActions()
                } else {
                    self.performRegistrationAction(json: userJSON)
                }
            })
        }
    }
    
    private func checkIfRegistered(id: String, token: String, completion: @escaping (Bool?) -> Void) {
        let params: [String: AnyObject] = ["access_token": token as AnyObject,
                                           "social_network": "FB" as AnyObject,
                                           "social_id": id as AnyObject]
        
        _ = CHAPI.request(.get,
                          CHMethod.IsRegisteredSocial,
                          params: params,
                          hashed: false,
                          error: { _ in
                            completion(nil)
        },
                          success: { json in
                            completion(json.bool)
        })
    }
    
    private func performLoginActions() {
        let token = FBSDKAccessToken.current()
        let id = token?.userID
        let tokenString = token?.tokenString
        User.shared.socialToken = tokenString
        User.shared.socialID = id
        let deviceID = UIDevice.current.identifierForVendor?.uuidString ?? ""
        
        
        let params: [String: AnyObject] = ["device_id": deviceID as AnyObject,
                                           "access_token": (tokenString ?? "") as AnyObject,
                                           "social_network": "FB" as AnyObject,
                                           "social_id": (id ?? "") as AnyObject]
        _ = CHAPI.request(.put,
                          CHMethod.SocialLoginMethod,
                          params: params,
                          hashed: false,
                          error: { _ in
                            //TODO: Handle error
        },
                          success: { json in
                            self.fillLoginInfo(json: json[0])
                            self.delegate?.loginCompleted()
        })
        
    }
    
    private func performRegistrationAction(json: JSON) {
        User.shared.fillFromFB(json: json)
        setupUserCity(json: json)
        
        if let email = User.shared.email {
            let params: [String: AnyObject] = ["field": "email" as AnyObject,
                                               "value": email as AnyObject]
            _ = CHAPI.request(.get,
                              CHMethod.UserDataAvailable,
                              params: params,
                              hashed: false,
                              error: { _ in
                                //TODO: Handle error
            },
                              success: { json in
                                let available = json.bool ?? false
                                if available {
                                    self.delegate?.shouldStartRegistration()
                                } else {
                                    //TODO: Handle error - user email exists
                                }
            })
        } else {
            self.delegate?.shouldStartRegistration()
        }
    }
    
    private func fillLoginInfo(json: JSON) {
        let hash = json["hash"].stringValue
        let userID = json["user_id"].intValue
        
        User.shared.userID = userID
        UserDefaultsManager().setHash(hash: hash)
        UserDefaultsManager().setUserID(userID: userID)
    }
    
    private func setupUserCity(json: JSON) {
        guard let location = json["location"]["name"].string else {
            return
        }
        guard !location.replacingOccurrences(of: " ", with: "").isEmpty else {
            return
        }
        
        let filter = GMSAutocompleteFilter()
        filter.type = .city
        GMSPlacesClient.shared().autocompleteQuery(location, bounds: nil, filter: filter) { (results, error) in
            guard error == nil else {
                return
            }
            
            if let cities = results, !cities.isEmpty {
                User.shared.city = cities[0].attributedPrimaryText.string
                User.shared.cityID = cities[0].placeID
                User.shared.cityGoogleID = cities[0].placeID
            }
        }
    }
}

protocol FacebookLoginManagerDelegate: class {
    func loginCompleted()
    func shouldStartRegistration()
}
