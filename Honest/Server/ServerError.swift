//
//  ServerError.swift
//  Choice
//
//  Created by admin on 01.07.16.
//  Copyright © 2016 STANISLAV SVOROVSKY. All rights reserved.
//

import Foundation

enum ServerError: Int {
    case noError = 0
    // api errors
    case brokenSession = 1 // invalid hash
    case vkUnAuthorized = 2
    case fbUnAuthorized = 3
    case badCredentials = 4 //invalid login-password
    case methodNotAllowed = 5
    case notValidPagination = 6
    case missingParameters = 7
    case incorrectFeedType = 8
    case unknownFilterName = 9 // in statistics
    case noSuchPoll = 10
    case statisticsClosed = 11
    case userWithSameEmailOrLogin = 12 // register or update-profile
    case logoutFromWrongDevice = 13
    case twiceReport = 14
    case repeatedSubscriptionToTheme = 15 //or unsubscribtion
    case invalidIsAvailableField = 16
    case selfSubscription = 17
    case repeatedSubscriptionToPeople = 18
    case invalidPassword = 19 // While updating password
    case noSuchComment = 20
    case twiceDeleteComment = 21
    case deleteForeignComment = 22
    case incorrectEventType = 23
    case voteOnClosedPoll = 24
    case voteOnUnexistedOption = 25
    case voteOnForeignOption = 26
    case repeatedVote = 27
    case closeOpenForeignStatistics = 28
    case repeatedDirectMessageToSameUser = 29
    case tooManyDirects = 30
    case noSuchUser = 31
    
    
    case noSocialNetworkAccountConnected = 32
    case socialNetworkError = 33
    
    // connection errors
    case timeout = 101
    case badResponse = 100
    case serverError = 105
    case badConnection = 111
    
    case unauthorized = -1
    case noHash = 77
    
    // unknown
    case unknownError = 99
    
    var error: CHError {
        return CHError( self )
    }
    
    var value: Int {
        return rawValue
    }
}

enum StatusCode: Int {
    case success = 200
    case badRequest = 400           // Pagination, Missing params
    case unauthorized = 401         // When invalid Hash
    case forbidden = 403            // Almost always
    case notFound = 404             // If not such method at all, Not such poll
    case methodNotAllowed = 405     // e.g. when POST instead of GET
    case notAcceptable = 406        // e.g. Invalid type of feed, Unknown filter
    case locked = 423               // e.g. Statistics closed
    case internalServerError = 500  // Server Error
    
    
}
