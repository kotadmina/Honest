//
//  String+Extension.swift
//  Honest
//
//  Created by Sher Locked on 26.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import Foundation

extension String {
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
