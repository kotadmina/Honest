//
//  FontLibrary.swift
//  Honest
//
//  Created by Sher Locked on 26.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import UIKit

class FontLibrary {
    
    
    static func geometricaRegular(size: CGFloat) -> UIFont {
        let font = UIFont(name: "GeometricaSans-Regular", size: size)!
        return font
    }
    
    static func nextartThin(size: CGFloat) -> UIFont {
        let font = UIFont(name: "NEXTART-Thin", size: size)!
        return font
    }
    
    static func geometriaRegular(size: CGFloat) -> UIFont {
        let font = UIFont(name: "Geometria", size: size)!
        return font
    }
    
    static func geometriaLight(size: CGFloat) -> UIFont {
        let font = UIFont(name: "Geometria-Light", size: size)!
        return font
    }
    
    static func geometriaMedium(size: CGFloat) -> UIFont {
        let font = UIFont(name: "Geometria-Medium", size: size)!
        return font
    }
}
