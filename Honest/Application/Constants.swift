//
//  Constants.swift
//  Honest
//
//  Created by Sher Locked on 26.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import Foundation

struct Constants {
    static let facebookAppID = "1224221644284489"
    static let facebookLoginPermissions = ["public_profile", "email", "user_friends", "user_birthday", "user_location"]
    static let apiUrl = "http://static.choiceapp.ru/api_url"
}
