//
//  EnterScreenScrollableViewModel.swift
//  Honest
//
//  Created by Sher Locked on 26.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import Foundation

struct EnterScreenScrollableViewModel {
    let imageName: String
    let title: String
    let description: String
}
