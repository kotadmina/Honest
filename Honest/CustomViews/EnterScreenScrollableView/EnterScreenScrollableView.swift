//
//  ScrollViewCell.swift
//  Honest
//
//  Created by Sher Locked on 25.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import UIKit

class EnterScreenScrollableView: UIView {

    @IBOutlet weak var scrollImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    func configure(with viewModel: EnterScreenScrollableViewModel) {
        scrollImage.image = UIImage(named: viewModel.imageName)
        titleLabel.text = viewModel.title.uppercased()
        descriptionLabel.text = viewModel.description
        stylize()
    }
    
    private func stylize() {
        titleLabel.font = FontLibrary.geometriaLight(size: 43)
        descriptionLabel.font = FontLibrary.geometriaRegular(size: 14)
        
        titleLabel.textColor = UIColor.black
        descriptionLabel.textColor = UIColor.black
    }
}
