//
//  SegmentView.swift
//  SegmentButton
//
//  Created by Sher Locked on 31.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import UIKit

class SegmentView: UIView, SegmentButtonDelegate {

    @IBOutlet weak var leftButton: SegmentButton!
    @IBOutlet weak var rightButton: SegmentButton!
    
    class func instanceFromNib() -> SegmentView {
        let view = Bundle.main.loadNibNamed("SegmentView", owner: nil, options: nil)?.first as! SegmentView
        view.configure()
        return view
    }
    
    enum SegmentViewState {
        case left
        case right
        case none
    }
    
    var state: SegmentViewState = .none {
        didSet {
            switch state {
            case .left:
                leftButton.state = .on
                rightButton.state = .off
            case .right:
                leftButton.state = .off
                rightButton.state = .on
            case .none:
                leftButton.state = .off
                rightButton.state = .off
            }
        }
    }
    
    func configure() {
        leftButton.state = .off
        rightButton.state = .off
        leftButton.delegate = self
        rightButton.delegate = self
    }
    
    func valueChanged(button: SegmentButton) {
        if button == leftButton {
            state = .left
        } else {
           state = .right
        }
    }
    
}
