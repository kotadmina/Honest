//
//  SegmentButton.swift
//  SegmentButton
//
//  Created by Sher Locked on 31.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import UIKit

@IBDesignable class SegmentButton: UIView {
    
    @IBInspectable var orientation: Bool = false {
        didSet {
            buttonLayer?.path = createPath().cgPath
        }
    }
    @IBInspectable var onColor: UIColor = .red {
        didSet {
            updateColor()
        }
    }
    
    @IBInspectable var offColor: UIColor = .red {
        didSet {
            updateColor()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 9 {
        didSet {
            buttonLayer?.path = createPath().cgPath
        }
    }
    
    @IBInspectable var text: String = "" {
        didSet {
            label?.text = text
        }
    }
    
    weak var delegate: SegmentButtonDelegate?
    
    var state: SegmentButtonState = .off {
        didSet {
            updateColor()
        }
    }
    
    var buttonLayer: CAShapeLayer!
    var label: UILabel!
    
    enum SegmentButtonState {
        case on
        case off
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createButton()
        createLabel()
        configureTap()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createButton()
        createLabel()
        configureTap()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        buttonLayer?.path = createPath().cgPath
        label?.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 18)
        label?.center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
    }
    
    private func createButton() {
        if buttonLayer == nil {
            buttonLayer = CAShapeLayer()
            buttonLayer.path = createPath().cgPath
            updateColor()
            buttonLayer.frame = self.layer.bounds
            self.layer.addSublayer(buttonLayer)
        }
    }
    
    private func configureTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.addGestureRecognizer(tap)
    }
    
    @objc private func handleTap() {
        switchState()
    }
    
    private func updateColor() {
        switch state {
        case .on:
            buttonLayer?.fillColor = onColor.cgColor
        case .off:
            buttonLayer?.fillColor = offColor.cgColor
        }
    }
    
    func switchState() {
        switch state {
        case .on:
            state = .off
        case .off:
            state = .on
        }
        delegate?.valueChanged(button: self)
    }
    
    private func createLabel() {
        if label == nil {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 18))
            label.center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
            label.textAlignment = .center
            label.text = text
            self.addSubview(label)
        }
    }
    
    private func createPath() -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: self.frame.width, y: 0))
        if orientation {
            path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height))
            path.addLine(to: CGPoint(x: cornerRadius, y: self.frame.height))
            path.addArc(withCenter: CGPoint(x: cornerRadius, y: self.frame.height - cornerRadius),
                        radius: cornerRadius,
                        startAngle: .pi / 2,
                        endAngle: .pi,
                        clockwise: true)
        } else {
            path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height - cornerRadius))
            path.addArc(withCenter: CGPoint(x: self.frame.width - cornerRadius, y: self.frame.height - cornerRadius),
                        radius: cornerRadius,
                        startAngle: 0,
                        endAngle: .pi / 2,
                        clockwise: true)
            path.addLine(to: CGPoint(x: 0, y: self.frame.height))
        }
        path.addLine(to: CGPoint(x: 0, y: 0))
        return path
    }
    
}

protocol SegmentButtonDelegate: class {
    func valueChanged(button: SegmentButton)
}
