//
//  ExpandableView.swift
//  ExpandableView
//
//  Created by Sher Locked on 30.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import UIKit

class ExpandableView: UIView {

    @IBOutlet weak var birthdayView: UIView!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var dateViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateViewBottomConstraint: NSLayoutConstraint!
    
    enum ExpandableViewState {
        case opened
        case closed
    }
    
    var state: ExpandableViewState = .closed
    var animationTime: Double = 0.5
    var selectedDate: Date {
        get {
            return datePicker.date
        }
        
        set {
            datePicker.date = newValue
            datePickerValueChanged()
        }
        
    }
    var dateFormatter = DateFormatter()
    
    class func loadFromNib() -> ExpandableView {
        let view =  Bundle.main.loadNibNamed("ExpandableView", owner: nil, options: nil)?.first as! ExpandableView
        view.configure()
        return view
    }
    
    func configure() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        self.birthdayView.addGestureRecognizer(tap)
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
    }
    
    @objc func datePickerValueChanged() {
        let dateValue = dateFormatter.string(from: selectedDate)
        birthdayLabel.text = dateValue
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        
        if state == .closed {
            dateViewTopConstraint.isActive = true
            dateViewBottomConstraint.isActive = false
            UIView.animate(withDuration: animationTime, animations: {
                self.arrowImage.transform = CGAffineTransform.identity
                self.layoutIfNeeded()
            })
            state = .opened
        } else {
            dateViewTopConstraint.isActive = false
            dateViewBottomConstraint.isActive = true
            UIView.animate(withDuration: animationTime, animations: {
                self.arrowImage.transform = CGAffineTransform(rotationAngle: -.pi)
                self.layoutIfNeeded()
            })
            state = .closed
        }
        
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        if state == .opened {
            return true
        } else {
            return birthdayView.frame.contains(point)
        }
    }

}
