//
//  FBCustomLoginButton.swift
//  Honest
//
//  Created by Sher Locked on 29.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import UIKit

class FBLoginButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 9
        self.backgroundColor = ColorsLibrary.buttonBlue
        self.setTitleColor(UIColor.white, for: .normal)
        self.titleLabel?.font = FontLibrary.geometriaRegular(size: 14)
        self.titleLabel?.text = "enter_screen_login_button".localized()
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: -15, bottom: 0, right: 0)
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
    }
    
   
    

}
