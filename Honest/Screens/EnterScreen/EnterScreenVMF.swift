//
//  EnterScreenVMF.swift
//  Honest
//
//  Created by Sher Locked on 26.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import UIKit

class EnterScreenVMF {
    
    func createPages() -> [EnterScreenScrollableViewModel] {
        let firstPage = EnterScreenScrollableViewModel(imageName: "scroll_image1",
                                                       title: "application_name".localized(),
                                                       description: "enter_screen_page1_description".localized())
        let secondPage = EnterScreenScrollableViewModel(imageName: "scroll_image2",
                                                        title: "application_name".localized(),
                                                        description: "enter_screen_page2_description".localized())
        let thirdPage = EnterScreenScrollableViewModel(imageName: "scroll_image3", title: "application_name".localized(), description: "enter_screen_page3_description".localized())
        return [firstPage, secondPage, thirdPage]
    }
    
}
