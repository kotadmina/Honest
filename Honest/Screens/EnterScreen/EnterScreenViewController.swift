//
//  EnterScreenViewController.swift
//  Honest
//
//  Created by Sher Locked on 25.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class EnterScreenViewController: UIViewController {
    
    @IBOutlet weak var enterScrollView: UIScrollView!
    @IBOutlet weak var viewPageControl: UIPageControl!
    @IBOutlet weak var loginButton: FBLoginButton!
    
    var loadingVC: LoadingScreenViewController!
    let vmf = EnterScreenVMF()
    let loginManager = FacebookLoginManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        loginManager.delegate = self
        configureScrollView()
        configureLoadingScreen()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        loadViews()
    }
    
    func configureLoadingScreen() {
        let storyboard = UIStoryboard(name: "LoadingScreen", bundle: nil)
        loadingVC = storyboard.instantiateViewController(withIdentifier: "LoadingScreen") as! LoadingScreenViewController
    }
    
    func configureScrollView() {
        enterScrollView.isPagingEnabled = true
        enterScrollView.showsHorizontalScrollIndicator = false
        enterScrollView.delegate = self
        enterScrollView.bounces = false

    }
    
    func loadViews() {
        let pages = vmf.createPages()
        enterScrollView.contentSize = CGSize(width: enterScrollView.frame.width * CGFloat(pages.count), height: enterScrollView.frame.height)
        for (index, page) in pages.enumerated() {
            if let scrollView = Bundle.main.loadNibNamed("EnterScreenScrollableView", owner: self, options: nil)?.first as? EnterScreenScrollableView {
                scrollView.configure(with: page)
                
                enterScrollView.addSubview(scrollView)
                scrollView.frame.size.width = enterScrollView.frame.width
                scrollView.frame.size.height = enterScrollView.frame.height
                scrollView.frame.origin.x = CGFloat(index) * enterScrollView.frame.width
            }
        }
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        let login = FBSDKLoginManager()
        login.logOut()
        login.loginBehavior = .native
        present(loadingVC, animated: true, completion: nil)
        
        let permissions = ["public_profile", "email", "user_friends", "user_birthday", "user_location"]
        
        login.logIn(withReadPermissions: permissions, from: loadingVC) {
            (result: FBSDKLoginManagerLoginResult!, error: Error!) -> Void in
            if (error != nil) || result.isCancelled {
                self.loadingVC.dismiss(animated: false, completion: nil)
            } else {
                self.loginManager.performLogin()
            }
        }
        
    }
}

extension EnterScreenViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x / scrollView.frame.size.width
        viewPageControl.currentPage = Int(page)
    }
}

extension EnterScreenViewController: FacebookLoginManagerDelegate {
    func shouldStartRegistration() {
        
    }
    
    func loginCompleted() {
        
    }
}
