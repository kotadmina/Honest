//
//  RegistrationScreenViewController.swift
//  Honest
//
//  Created by Sher Locked on 29.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import UIKit

class RegistrationScreenViewController: UIViewController {

    @IBOutlet weak var actionLabel: UILabel!
    @IBOutlet weak var expandableView: UIView!
    @IBOutlet weak var genderView: SegmentView!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    
    var subviewsConfigured = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavBar()
        nextButton.addTarget(self, action: #selector(x), for: .touchUpInside)
    }
    
    @objc func x() {
        leadingConstraint.isActive = false
        trailingConstraint.isActive = true
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if !subviewsConfigured {
            configureGenderView()
            configureExpandableView()
            subviewsConfigured = true
        }
    }
    
    func configureNavBar() {
        guard let nav = navigationController else {
            return
        }
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,
                                                                   NSAttributedStringKey.font: FontLibrary.geometriaMedium(size: 17)]
        nav.navigationBar.topItem?.title = "РЕГИСТРАЦИЯ"

        let label = UILabel()
        label.text = "1/2"
        label.font = FontLibrary.geometriaMedium(size: 14)
        label.textColor = UIColor.white
        let rightButton = UIBarButtonItem(customView: label)
        
        navigationItem.rightBarButtonItem = rightButton
    }
    
    func configureExpandableView() {
        let customView = ExpandableView.loadFromNib()
        customView.frame = expandableView.bounds
        if let birthDate = User.shared.birthDate {
            customView.selectedDate = birthDate
        } else {
            customView.birthdayLabel.text = "Дата рождения"
        }
        expandableView.addSubview(customView)
    }
    
    func configureGenderView() {
        let newView = SegmentView.instanceFromNib()
        newView.frame = genderView.bounds
        if User.shared.gender == .male {
            newView.state = .left
        } else if User.shared.gender == .female {
            newView.state = .right
        } else {
            newView.state = .none
        }
        genderView.addSubview(newView)
    }

}
