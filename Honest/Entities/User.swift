//
//  User.swift
//  Honest
//
//  Created by Sher Locked on 28.01.2018.
//  Copyright © 2018 InRight. All rights reserved.
//

import Foundation
import SwiftyJSON

class GenericUserInfo {
   
    var name: String?
    var surname: String?
    var nickname: String?
    var photoURL: URL?
    var userID: Int?
    var fullName: String {
        return (name ?? "") + " " + (surname ?? "")
    }
}

enum Gender: String {
    case female = "Female"
    case male = "Male"
    case notSet = "Not Set"
}

class RegistrationInfo: GenericUserInfo {
    
    var email: String?
    var birthDate: Date?
    var gender: Gender?
    var socialID: String?
    var socialToken: String?
    var city: String?
    var cityID: String?
    var cityGoogleID: String?
    var country: String?
}

struct UserCounters {
    var rating: Int
    var questions: Int
    var subscribers: Int
    var subscriptions: Int
}

class User: RegistrationInfo {
    var counters: UserCounters?
    static let shared = User()
    
    func fillFromFB(json: JSON) {
        name = json["first_name"].string
        surname = json["last_name"].string
        nickname = nil
        email = json["email"].string
        socialID = json["id"].string
        let genderString = json["gender"].stringValue
        switch genderString {
        case "male":
            gender = .male
        case "female":
            gender = .female
        default:
            gender = .notSet
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        birthDate = dateFormatter.date(from: json["birthday"].stringValue)?.addingTimeInterval(12 * 60 * 60)
        
    }
}
